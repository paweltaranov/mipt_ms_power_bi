## Физтех-школа прикладной математики и информатики МФТИ

## Итоговый проект курса «Инструменты анализа и визуализации данных»

Задача: Реализовать интерактивную отчетность в BI-системе

Инструмент: MS Power BI 

**Проект(скачать pbix-файл): [Online_Sales_in_USA_FinalProject](Online_Sales_in_USA_FinalProject.pbix)** 

Источник данных: [Online Sales in USA](https://www.kaggle.com/datasets/ytgangster/online-sales-in-usa) 

Общие указания: [TOR_Terms_of_reference](TOR_Terms_of_reference.pdfa)

Пояснительная записка: [Project_info](Project_info.pdf)

[Проект на https://app.powerbi.com/](https://app.powerbi.com/groups/a90ca769-5541-46ae-8380-39e54bdee95e/reports/20cebe79-59e8-4057-8fb1-e54e0f4539ba/ReportSection579597d079ae71b02845?redirectedFromSignup=1)

[![Typing SVG](https://readme-typing-svg.herokuapp.com?color=%2336BCF7&lines=Дашборд+Online+Sales+in+USA)](https://git.io/typing-svg) 
![Dashboard](/Datasets/Dashboard_title.jpg)

![Microsoft](https://img.shields.io/badge/Microsoft-0078D4?style=for-the-badge&logo=microsoft&logoColor=white)
![Power Bi](https://img.shields.io/badge/power_bi-F2C811?style=for-the-badge&logo=powerbi&logoColor=black)
![Microsoft Visio ](https://img.shields.io/badge/Microsoft_Visio-3955A3?style=for-the-badge&logo=microsoft-visio&logoColor=white)
![Microsoft Learn](https://img.shields.io/badge/Microsoft_Learn-258ffa?style=for-the-badge&logo=microsoft&logoColor=white)
![Kaggle](https://img.shields.io/badge/Kaggle-035a7d?style=for-the-badge&logo=kaggle&logoColor=white)

<!-- ![](https://komarev.com/ghpvc/?username=paweltaranov) -->
